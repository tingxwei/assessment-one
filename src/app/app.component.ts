import { Component, OnInit } from '@angular/core';
import { UserInfo } from './shared/user-info';
import { InfoUserService} from './services/info-user.service';
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  users: any;
  title = 'app';
  model = null;
  gender: string[] = ['M', 'F'];
  countries = [
    { desc: 'Singaporean', value: 'SG' },
    { desc: 'Malaysian', value: 'MY' },
    { desc: 'Thai', value: 'TH' },
    { desc: 'Vietnamese', value: 'VN' }];

  isSubmitted: boolean = false;

  constructor(private userService: InfoUserService){

    
  }
 

  ngOnInit() {
    this.model = new UserInfo('', '', '', '', '', '', null, '', 'SG', '');
  }

  onSubmit() {
    console.log(this.model.email);
    console.log(this.model.password);
    console.log(this.model.confirmPassword);
    console.log(this.model.firstName);
    console.log(this.model.lastName);
    console.log(this.model.gender);
    console.log(this.model.dateOfBirth);
    console.log(this.model.address);
    console.log(this.model.country);
    console.log(this.model.contactNumber);
    
    this.isSubmitted = true;
    
    this.userService.saveInfoUser(this.model)
      .subscribe(users => {
        console.log('send to backend !');
        console.log(users);
        this.users = users;
      })
  }
}





