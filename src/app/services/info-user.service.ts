import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { UserInfo } from '../shared/user-info';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class InfoUserService {
  private userAPIURL = `${environment.backendApiURL}/api/user/` ;
 
  
  constructor(private httpClient: HttpClient) { }


  public saveInfoUser(user: UserInfo) : Observable<UserInfo>{
    return this.httpClient.post<UserInfo>(this.userAPIURL + 'register', user, httpOptions); 
}
}