export class UserInfo {
    constructor(
        public email: string,
        public password: string,
        public confirmPassword: string,
        public firstName: string,
        public lastName: string,
        public gender: string,
        public dateOfBirth: Date,
        public address: string,
        public country: string,
        public contactNumber: string,
        
       
    ){

    }

}
